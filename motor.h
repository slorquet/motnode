#ifndef __MOTNODE__MOTOR__H__
#define __MOTNODE__MOTOR__H__

#include <fixedmath.h>
#include "encoder.h"
#include "pwm.h"
#include "pid.h"

#define MOTNODE_CONTROL_OPENLOOP 0 /* direct pwm value applied, no control, not even acceleration. Input is command. */
#define MOTNODE_CONTROL_SPEED    1 /* only speed is controlled. Input is v_setpt_tgt. */
#define MOTNODE_CONTROL_PVT      2 /* Unsupported for now, speed and position are controlled */

#define MOTNODE_DIR_STOPPED 0 /* Motor is disabled, no command applied */
#define MOTNODE_DIR_CW      1 /* Motor turns clockwise, pulses increase, zero speed means brake */
#define MOTNODE_DIR_CCW     2 /* Motor turns counterclockwise, pulses decrease, zero speed means brake */

struct motnode_tlm_s;

struct motnode_motor_s
{
  FAR struct motnode_encoder_s   *encoder;
  FAR struct motnode_pwm_s       *pwm;
  FAR struct motnode_tlm_s       *tlm;

  struct motnode_pid_s pid_speed;
  
  /* Settings */
  
  b16_t    accel;         /* Acceleration in pulses/tick/tick */
  b16_t    decel;         /* Deceleration in pulses/tick/tick */
  uint32_t freq;          /* PWM frequency */

  /* Command */
  
  int     mode;           /* Current control mode */
  b16_t   v_setpt_tgt;    /* Target speed to be reached in pulses/tick, not used in open loop mode */
  int     direction;      /* Movement requested direction */

  /* Input State */
  
  int32_t newpos;         /* Current encoder position in pulses */
  int32_t oldpos;         /* Previous encoder position in pulses */
  b16_t   v_setpt_cur;    /* Current speed setpoint, temporary speed while ramping, in pulses/tick */
  b16_t   rate;           /* Current speed in pulses/tick, measured from encoder */

  /* Output state */

  b16_t   command;        /* Current PWM rate sent to motor */
};

#endif /* __MOTNODE__MOTOR__H__ */
