/****************************************************************************
 * motnode/console.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <sys/ioctl.h>

#include <fixedmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <debug.h>

#include "console.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

typedef int (*cmdfn)(FAR struct motnode_console_s *cons, FAR char *cmd);

struct command
{
  const char *name;
  cmdfn fn;
};

struct motnode_console_s
{
  FAR FILE *in;
  FAR FILE *out;
  char buf[128];
  bool running;
  FAR struct motnode_motor_s *motor;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

static int fn_exit  (FAR struct motnode_console_s *cons, FAR char *cmd);
static int fn_stat  (FAR struct motnode_console_s *cons, FAR char *cmd);
static int fn_acc   (FAR struct motnode_console_s *cons, FAR char *cmd);
static int fn_pwm   (FAR struct motnode_console_s *cons, FAR char *cmd);
static int fn_mode  (FAR struct motnode_console_s *cons, FAR char *cmd);
static int fn_speed (FAR struct motnode_console_s *cons, FAR char *cmd);
static int fn_spid  (FAR struct motnode_console_s *cons, FAR char *cmd);

/****************************************************************************
 * Private Data
 ****************************************************************************/

const struct command commands[] = 
{
  {"exit" , fn_exit },
  {"stat" , fn_stat },
  {"acc"  , fn_acc  },
  {"pwm"  , fn_pwm  },
  {"mode" , fn_mode },
  {"speed", fn_speed},
  {"spid" , fn_spid },
};

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************/
static int fn_exit(FAR struct motnode_console_s *cons, FAR char *cmd)
{
  cons->running = false;
  return 0;
}

/****************************************************************************/
static int fn_stat(FAR struct motnode_console_s *cons, FAR char *cmd)
{
  int32_t pos;
  uint32_t freq;
  bool state;
  b16_t duty[2];
  
  fprintf(cons->out, "ACC      %.3f pulses/tick^2\n", b16tof(cons->motor->accel));
  fprintf(cons->out, "DEC      %.3f pulses/tick^2\n", b16tof(cons->motor->decel));
  motnode_encoderposition(cons->motor->encoder, &pos);
  fprintf(cons->out, "ENC pos  %d pulses\n", pos);
  fprintf(cons->out, "ENC rate %d pulses/tick\n", cons->motor->rate);
  motnode_pwmgetstate(cons->motor->pwm, &state);
  motnode_pwmgetfreq(cons->motor->pwm, &freq);
  fprintf(cons->out, "PWM %s  freq %d Hz\n", state?" ON":"OFF", freq);
  motnode_pwmgetrates(cons->motor->pwm, &duty[0], &duty[1]);
  fprintf(cons->out, "PWM duty %.3f, %.3f\n",b16tof(duty[0]), b16tof(duty[1]));
  return 0;
}

/****************************************************************************/
static int fn_mode (FAR struct motnode_console_s *cons, FAR char *cmd)
{
  if(!strlen(cmd))
    {
      fprintf(cons->out, "Current mode: %s\n", cons->motor->mode==MOTNODE_CONTROL_OPENLOOP?"Open-loop":(cons->motor->mode==MOTNODE_CONTROL_SPEED?"Speed":"PVT"));
      return 0;
    }
  if(!strcmp(cmd," open"))
    {
      cons->motor->mode = MOTNODE_CONTROL_OPENLOOP;
      fprintf(cons->out, "Switched to Open loop mode\n");
    }
  else if(!strcmp(cmd," speed"))
    {
      cons->motor->mode = MOTNODE_CONTROL_SPEED;
      fprintf(cons->out, "Switched to Speed mode\n");
    }
  else if(!strcmp(cmd," pvt"))
    {
      cons->motor->mode = MOTNODE_CONTROL_PVT;
      fprintf(cons->out, "Switched to PVT mode\n");
    }
  else
    {
      fprintf(cons->out, "mode [open|speed|pvt]\n");
    }

  return 0;
}

/****************************************************************************/
static int fn_pwm(FAR struct motnode_console_s *cons, FAR char *cmd)
{
  float val;

  if(cons->motor->mode != MOTNODE_CONTROL_OPENLOOP)
    {
      fprintf(cons->out, "Motor must be in open loop mode\n");
      goto done;
    }

  if(!strlen(cmd))
    {
      return fn_stat(cons, cmd);
    }

  if (*cmd != ' ')
    {
      fprintf(cons->out, "Bad command\n");
      goto help;
    }

  cmd++; /*skip space*/
  if(strlen(cmd)<1) /*letter*/
    {
      fprintf(cons->out, "Bad command\n");
      goto help;
    }

   if(cmd[0] == '0')
    {
      cons->motor->command = 0;
      cons->motor->direction = MOTNODE_DIR_STOPPED;
      fprintf(cons->out, "OK disabling control\n");
      goto done;
    }

  if(strlen(cmd)<2) /*letter and a number*/
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }

  if(sscanf(cmd+1, "%f", &val)!=1)
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }
  else if(cmd[0] == '+')
    {
      if(val<0 || val>=100)
        {
          fprintf(cons->out, "Invalid value\n");
          goto help;
        }
      cons->motor->direction = MOTNODE_DIR_CW;
      cons->motor->command = ftob16(val/100.0);
      fprintf(cons->out, "OK pwm CW %.3f [%08x]\n", val, cons->motor->command);
    }
  else if(cmd[0] == '-')
    {
      if(val<0 || val>=100)
        {
          fprintf(cons->out, "Invalid value\n");
          goto help;
        }
      cons->motor->direction = MOTNODE_DIR_CCW;
      cons->motor->command = ftob16(val/100.0);
      fprintf(cons->out, "OK pwm CCW %.3f [%08x]\n", val, cons->motor->command);
    }
  else if(cmd[0] == 'f')
    {
      if(val<1000)
        {
          fprintf(cons->out, "Invalid value\n");
          goto help;
        }
      cons->motor->freq = (uint32_t)val;
      fprintf(cons->out, "OK changing frequency: %u Hz\n", cons->motor->freq);
    }
  else
    {
help:
      fprintf(cons->out, "pwm [0|+ddd|-ddd]\n");
      return ERROR;
    }

done:
  return 0;
}

/****************************************************************************/
static int fn_acc(FAR struct motnode_console_s *cons, FAR char *cmd)
{
  float val;

  if(!strlen(cmd))
    {
      fprintf(cons->out, "Current Accel: %.3f pulses/tick^2, Decel: %.3f pulses/tick^2\n", b16tof(cons->motor->accel), b16tof(cons->motor->decel));
      return 0;
    }

  if (*cmd != ' ')
    {
      fprintf(cons->out, "Bad command\n");
      goto help;
    }

  cmd++; /*skip space*/
  if(strlen(cmd)<2) /*sign and a number*/
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }

  if(sscanf(cmd+1, "%f", &val)!=1)
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }

  if(cmd[0] == '+')
    {
      cons->motor->accel = ftob16(val);
      fprintf(cons->out, "OK pos accel=%.3f, [%08X]\n",val, cons->motor->accel);
    }
  else if(cmd[0] == '-')
    {
      cons->motor->decel = ftob16(val);
      fprintf(cons->out, "OK neg accel=%.3f, [%08X]\n",val, cons->motor->decel);
    }
  else
    {
help:
      fprintf(cons->out, "acc [+acc|-dec]\n");
      return ERROR;
    }

  return 0;
}

/****************************************************************************/
static int fn_speed (FAR struct motnode_console_s *cons, FAR char *cmd)
{
  float val;

  if(cons->motor->mode != MOTNODE_CONTROL_SPEED)
    {
      fprintf(cons->out, "Motor must be in speed mode\n");
      goto done;
    }

  if(!strlen(cmd))
    {
      fprintf(cons->out, "Current speed target: %.3f, Current speed setpoint: %.3f\n", b16tof(cons->motor->v_setpt_tgt), b16tof(cons->motor->v_setpt_cur));
      return 0;
    }

  if (*cmd != ' ')
    {
      fprintf(cons->out, "Bad command\n");
      goto help;
    }

  cmd++; /*skip space*/
  if(strlen(cmd)<1) /*letter*/
    {
      fprintf(cons->out, "Bad command\n");
      goto help;
    }

  if(cmd[0] == '0')
    {
      cons->motor->direction = MOTNODE_DIR_STOPPED;
      cons->motor->v_setpt_tgt = 0;
      goto done;
    }

  if(strlen(cmd)<2) /*letter and a number*/
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }

  if(sscanf(cmd+1, "%f", &val)!=1)
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }
  else if(cmd[0] == '+')
    {
      if(cons->motor->direction == MOTNODE_DIR_CCW)
        {
          fprintf(cons->out, "Bad direction, CW required\n");
          goto help;
        }
      cons->motor->direction = MOTNODE_DIR_CW;
      cons->motor->v_setpt_tgt = ftob16(val);
      fprintf(cons->out, "OK speed CW %.3f [%08x] \n", val, cons->motor->v_setpt_tgt);
      goto done;
    }
  else if(cmd[0] == '-')
    {
      if(cons->motor->direction == MOTNODE_DIR_CW)
        {
          fprintf(cons->out, "Bad direction, CCW required\n");
          goto help;
        }
      cons->motor->direction = MOTNODE_DIR_CCW;
      cons->motor->v_setpt_tgt = ftob16(val);
      fprintf(cons->out, "OK speed CCW %.3f [%08x] \n", val, cons->motor->v_setpt_tgt);
      goto done;
    }

  else
    {
help:
      fprintf(cons->out, "speed [0|+ddd|-ddd]\n");
      return ERROR;
    }

done:
  return 0;
}

/****************************************************************************/
static int autotune(FAR struct motnode_console_s *cons)
{
  fprintf(cons->out, "Autotune not implemented yet\n");
  return 0;
}

/****************************************************************************/
/* spid [[p|i|d]val] */
static int fn_spid  (FAR struct motnode_console_s *cons, FAR char *cmd)
{
  float val;

  if(cons->motor->mode != MOTNODE_CONTROL_SPEED && cons->motor->mode != MOTNODE_CONTROL_PVT)
    {
      fprintf(cons->out, "Motor must be in speed mode\n");
      goto done;
    }

  if(!strlen(cmd))
    {
      fprintf( cons->out, "Speed controller P: %.3f, I: %.3f D: %.3f\n",
        b16tof(cons->motor->pid_speed.Kp),
        b16tof(cons->motor->pid_speed.Ki),
        b16tof(cons->motor->pid_speed.Kd));
      return 0;
    }

  if (*cmd != ' ')
    {
      fprintf(cons->out, "Bad command\n");
      goto help;
    }

  cmd++; /*skip space*/
  if(strlen(cmd)<1) /*letter*/
    {
      fprintf(cons->out, "Bad command\n");
      goto help;
    }

  if(cmd[0] == 'a')
    {
      return autotune(cons);
      goto done;
    }

  if(strlen(cmd)<2) /*letter and a number*/
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }

  if(sscanf(cmd+1, "%f", &val)!=1)
    {
      fprintf(cons->out, "Invalid value\n");
      goto help;
    }
  else if(cmd[0] == 'p')
    {
      if(val<0)
        {
        fprintf(cons->out, "Invalid value, must not be negative\n");
        return ERROR;
        }
      cons->motor->pid_speed.Kp = ftob16(val);
      fprintf(cons->out, "OK Kp = %.3f [%08x] \n", val, cons->motor->pid_speed.Kp);
      goto done;
    }
  else if(cmd[0] == 'i')
    {
      if(val<0)
        {
        fprintf(cons->out, "Invalid value, must not be negative\n");
        return ERROR;
        }
      cons->motor->pid_speed.Ki = ftob16(val);
      fprintf(cons->out, "OK Ki = %.3f [%08x] \n", val, cons->motor->pid_speed.Ki);
      goto done;
    }
  else if(cmd[0] == 'd')
    {
      if(val<0)
        {
        fprintf(cons->out, "Invalid value, must not be negative\n");
        return ERROR;
        }
      cons->motor->pid_speed.Kd = ftob16(val);
      fprintf(cons->out, "OK Kd = %.3f [%08x] \n", val, cons->motor->pid_speed.Kd);
      goto done;
    }
  else
    {
help:
      fprintf(cons->out, "pid [a|pxxx.x|ixxx.x|dxxx.x]\n");
      return ERROR;
    }

done:
  return 0;

}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************/
int motnode_consoleinit(FAR struct motnode_console_s **cons, FAR struct motnode_motor_s *motor)
{
  FAR struct motnode_console_s *c = malloc(sizeof(struct motnode_console_s));

  if (c == NULL)
    {
      fprintf(stderr, "Failed to alloc console\n");
      return ERROR;
    }

  fprintf(stderr, "Starting console\n");
  c->in    = stdin;
  c->out   = stdout;
  c->motor = motor;
 
  fflush(c->in);
  memset(c->buf, 0, sizeof(c->buf));

  *cons = c;
  
  return 0;
}

/****************************************************************************/
void motnode_consoleshutdown(FAR struct motnode_console_s *cons)
{
  fprintf(stderr, "Stopping console\n");
  fflush(cons->in);
}

/****************************************************************************/
void motnode_consoleloop(FAR struct motnode_console_s *cons)
{
  int len,ch,i;

  cons->running = true;
  
  while(cons->running)
    {
      fprintf(cons->out, "motnode> "); fflush(cons->out);
      len=0;
      while(true)
        {
          ch=fgetc(cons->in);
          if(ch=='\r') break;    /*CR*/
          if(ch=='\n') continue; /*LF*/
          if(len<(sizeof(cons->buf)-1))
            {
              fputc(ch,cons->out);
              fflush(cons->out);
              cons->buf[len++] = ch;
            }
          else break;
        }
      cons->buf[len]=0;
      fputc('\n', cons->out);
      for(i=0;i<(sizeof(commands)/sizeof(commands[0]));i++)
        {
          if(!strncmp(cons->buf, commands[i].name, strlen(commands[i].name)))
            {
              commands[i].fn(cons, cons->buf+strlen(commands[i].name));
              continue;
            }
        }
      fprintf(cons->out, "Command not found\n");
    }

}

