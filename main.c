/****************************************************************************
 * motnode/motnode_main.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <semaphore.h>

#include "encoder.h"
#include "pwm.h"
#include "timer.h"
#include "pid.h"
#include "console.h"
#include "motor.h"
#include "telemetry.h"

#ifdef CONFIG_MOTNODE_CANOPEN
#include "canopen.h"
#endif

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private types
 ****************************************************************************/

/****************************************************************************
 * Global variables
 ****************************************************************************/
struct motnode_timer_s   *timer;
struct motnode_encoder_s *encoder;
struct motnode_pwm_s     *pwm;
struct motnode_motor_s   motor;
struct motnode_tlm_s     *tlm;
struct motnode_console_s *cons;

#ifdef CONFIG_MOTNODE_CANOPEN
struct motnode_canopen_s can;
#endif

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************/
#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int motnode_main(int argc, FAR char *argv[])
#endif
{
  int ret = OK;

  /* -------------------- */

  /* Initialize Encoder */

  fprintf(stderr, "Starting encoder\n");
  ret = motnode_encoderinit(&encoder);

  if(ret)
    {
      fprintf(stderr, "Encoder start failed\n");
      goto close;
    }

  /* Initialize PWM */

  fprintf(stderr, "Starting encoder\n");
  ret = motnode_pwminit(&pwm);

  if(ret)
    {
      fprintf(stderr, "PWM start failed\n");
      goto closeencoder;
    }

  ret = motnode_pwmsetstate(pwm, false); /* Disable the PWM outputs, motor is freewheeling */

  if(ret)
    {
      fprintf(stderr, "PWM output disable failed\n");
      goto closeencoder;
    }

  ret = motnode_pwmsetfreq(pwm, CONFIG_MOTNODE_PWM_FREQ); /* Disable the PWM outputs, motor is freewheeling */

  if(ret)
    {
      fprintf(stderr, "PWM output disable failed\n");
      goto closeencoder;
    }

  /* Initialize Motor */

  motor.pwm       = pwm;
  motor.encoder   = encoder;
  motor.mode      = MOTNODE_CONTROL_OPENLOOP;
  motor.direction = MOTNODE_DIR_STOPPED;
 
  /* Initialize Telemetry */

  fprintf(stderr, "Starting telemetry\n");
  ret = motnode_tlminit(&tlm, &motor);

  if(ret)
    {
      fprintf(stderr, "Telemetry start failed\n");
      goto closepwm;
    }

  /* Initialize Periodic loop */

  fprintf(stderr, "Starting timer\n");
  ret = motnode_timerinit(&timer, &motor);

  if(ret)
    {
      fprintf(stderr, "Timer start failed\n");
      goto closetlm;
    }

#ifdef CONFIG_MOTNODE_CANOPEN
  /* Initialize CANopen background protocol */

  ret = motnode_canopeninit(&can);

  if(ret)
    {
      fprintf(stderr, "CAN init failed\n");
      goto closetimer;
    }
#endif

  /* Initialize command console */

  ret = motnode_consoleinit(&cons, &motor);

  if(ret)
    {
      fprintf(stderr, "Console init failed\n");
      goto closeall;
    }
  
  /* -------------------- */

  /* System is running */
  fprintf(stderr, "System Initialized\n");

  /* Do nothing except poll console commands */

  motnode_consoleloop(cons);

  /* -------------------- */

  /* Stop everything in order */

  motnode_consoleshutdown(cons);

closeall:

#ifdef CONFIG_MOTNODE_CANOPEN
  fprintf(stderr, "Stopping CANopen\n");
  motnode_canopenshutdown(&can);
#endif

closetimer:
  fprintf(stderr, "Stopping timer\n");
  motnode_timershutdown(timer);

closetlm:
  fprintf(stderr, "Stopping telemetry\n");
  motnode_tlmshutdown(tlm);

closepwm:
  fprintf(stderr, "Stopping PWM\n");
  motnode_pwmshutdown(pwm);

closeencoder:
  fprintf(stderr, "Stopping encoder\n");
  motnode_encodershutdown(encoder);
close:
  return ret;
}

