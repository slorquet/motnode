#ifndef __MOTNODE__TIMER__H__
#define __MOTNODE__TIMER__H__

#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <fixedmath.h>

struct motnode_timer_s
{
  int fd;
  pthread_t pid;
  bool running;
  FAR struct motnode_motor_s *motor;
  b16_t ratebuf[10]; /*used to average motor rate*/
};

int motnode_timerinit(FAR struct motnode_timer_s **tim, FAR struct motnode_motor_s *mot);
void motnode_timershutdown(FAR struct motnode_timer_s *tim);

#endif /* __MOTNODE__TIMER__H__ */

