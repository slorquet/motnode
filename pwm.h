#ifndef __MOTNODE__PWM__H__
#define __MOTNODE__PWM__H__

#include <stdbool.h>
#include <fixedmath.h>

struct motnode_pwm_s;

int motnode_pwminit(FAR struct motnode_pwm_s **pwm);
void motnode_pwmshutdown(FAR struct motnode_pwm_s *pwm);

int motnode_pwmsetfreq(FAR struct motnode_pwm_s *pwm, uint32_t freq);
int motnode_pwmgetfreq(FAR struct motnode_pwm_s *pwm, FAR uint32_t *freq);

int motnode_pwmsetrates(FAR struct motnode_pwm_s *pwm, b16_t rate0, b16_t rate1);
int motnode_pwmgetrates(FAR struct motnode_pwm_s *pwm, FAR b16_t *rate0, FAR b16_t *rate1);

int motnode_pwmsetstate(FAR struct motnode_pwm_s *pwm, bool enabled);
int motnode_pwmgetstate(FAR struct motnode_pwm_s *pwm, FAR bool *enabled);

#endif /* __MOTNODE__PWM__H__ */

