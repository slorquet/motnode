#!/usr/bin/python

import serial
import sys
import struct

def rx(buf):
	if len(buf) != 22: return

	data = struct.unpack('<IiiIIH', buf)
	print "%8d enc %d/%d flg %s %s %s %s rate %d" % (
			data[0],
			data[1],data[2],
			"ON" if ((data[3]&1)==1) else "OFF",
			"CW" if ((data[3]&2)==2) else "CCW", 
			"CLOSED" if ((data[3]&4)==4) else "OPEN",
			"PVT" if ((data[3]&8)==8) else "SPEED",
			data[4])

s = serial.Serial(sys.argv[1], 1000000)
s.flushInput()

state=0
while True:
	c = s.read()
	#print ord(c),
	if state==0: #wait
		if c=='\x7E':
			state = 1
			buf=""

	elif state==1: #sync
		if c!='\x7E':
			state = 2
			buf = buf + c

	elif state==2: #rx
		if c=='\x7E':
			state = 0
			rx(buf)
		elif c=='\x7D':
			state = 2
		else:
			buf=buf + c

	elif state==3: #escape
		buf = buf + chr(ord(c) ^ 0x20)
		state = 1
