#!/usr/bin/env python
#telemetry reception

import Tkinter as tk
import sys
import math
import serial
import threading
import struct

class tlm(tk.Toplevel):
  def __init__(self,root,w):
    root.title("Telemetry")

    self.ratebuf   = []
    self.rateseg   = []
    self.cmdbuf   = []
    self.cmdseg   = []
    self.ssptbuf   = []
    self.ssptseg   = []
    self.index = 0
    self.step  = 0
    self.size  = w
    
    self.canvas = tk.Canvas(root,width=w,height=w*3/4)
    self.canvas.grid(row=0, column=2, rowspan=7)
    for i in range(w):
      self.ratebuf.append(0)
      self.cmdbuf.append(0)
      self.ssptbuf.append(0)
      if i<w-1:
        self.rateseg.append(self.canvas.create_line(i,0,i+1,0))
        self.cmdseg.append(self.canvas.create_line(i,0,i+1,0,fill="red"))
        self.ssptseg.append(self.canvas.create_line(i,0,i+1,0,fill="green"))

    tk.Label(root, text="Position").grid(row=0, column=0)
    self.encpos = tk.DoubleVar()
    tk.Entry(root, textvariable=self.encpos).grid(row=0, column=1)
    
    tk.Label(root, text="Rate").grid(row=1, column=0)
    self.encrate = tk.DoubleVar()
    tk.Entry(root, textvariable=self.encrate).grid(row=1, column=1)

    tk.Label(root, text="Mode").grid(row=2, column=0)
    self.mode = tk.StringVar()
    tk.Entry(root, textvariable=self.mode).grid(row=2, column=1)

    tk.Label(root, text="Status").grid(row=3, column=0)
    self.enabled = tk.StringVar()
    tk.Entry(root, textvariable=self.enabled).grid(row=3, column=1)

    tk.Label(root, text="Command").grid(row=4, column=0)
    self.command = tk.DoubleVar()
    tk.Entry(root, textvariable=self.command).grid(row=4, column=1)

    tk.Label(root, text="Direction").grid(row=5, column=0)
    self.direction = tk.StringVar()
    tk.Entry(root, textvariable=self.direction).grid(row=5, column=1)
    
    tk.Label(root, text="Speed setpoint").grid(row=6, column=0)
    self.vsetpt = tk.DoubleVar()
    tk.Entry(root, textvariable=self.vsetpt).grid(row=6, column=1)

  def update(self, tup):
    self.encpos.set(tup[1])
    self.encrate.set(tup[2])
    self.command.set(tup[4])
    self.direction.set("CW" if ((tup[3]&2)==2) else "CCW")
    self.mode.set("OPEN" if ((tup[3]&4)!=4) else ("PVT" if ((tup[3]&8)==8) else "SPEED"))
    self.enabled.set("ON" if ((tup[3]&1)==1) else "OFF")
    self.vsetpt.set(tup[5])
    self.push(tup)
        
  def push(self,tup):
    i=self.index

    self.ratebuf[i] = tup[2] * 0.8

    cmd = tup[4]
    if ((tup[3]&2)==0): cmd = -cmd
    self.cmdbuf[i] = cmd

    self.ssptbuf[i] = tup[5] * 0.8
    
    h=(self.size * 3/4)/2

    self.index = i + 1
    if self.index==(self.size): self.index=0

    self.step += 1
    if self.step==10: self.step=0
    
    #redraw
    if self.step==0:
      col=self.index
      for x in range(self.size-1):
        col -= 1
        if col>(self.size-2):col=0
        if col<0:col=self.size-2
        self.canvas.coords(self.rateseg[x] ,x,h-self.ratebuf[col],x-1,h-self.ratebuf[col+1])
        self.canvas.coords(self.cmdseg[x]  ,x,h-self.cmdbuf [col],x-1,h-self.cmdbuf [col+1])
        self.canvas.coords(self.ssptseg[x] ,x,h-self.ssptbuf[col],x-1,h-self.ssptbuf[col+1])

class Receiver:
  def __init__(self, portname, gui):
    self.portname = portname
    self.serial   = serial.Serial(portname, 1000000, timeout=0)
    self.state    = 0
    self.gui      = gui
    self.grstep   = 0
  
  def rxpacket(self,buf):
    if len(buf) != 26: return

    data = list(struct.unpack('<IiiIIIH', buf))
    #transform b16_t to float
    data[2] /= 65536.0 #rate
    data[4] = (data[4] *100) / 65536.0 #command, to %
    data[5] /= 65536.0 #setpoint

    self.grstep += 1
    if self.grstep==10:
      self.grstep=0
      self.gui.update(data)
    
    #print "%8d enc %d/%d flg %s %s %s %s rate %d vsetpt" % (
    #  data[0],
    #  data[1],data[2],
    #  "ON" if ((data[3]&1)==1) else "OFF",
    #  "CW" if ((data[3]&2)==2) else "CCW", 
    #  "CLOSED" if ((data[3]&4)==4) else "OPEN",
    #  "PVT" if ((data[3]&8)==8) else "SPEED",
    #  data[4],
    #  data[5])

  def manage(self, char):
    #print ord(char),
    if self.state==0: #wait
      if char=='\x7E':
        self.state = 1
        self.buf=""

    elif self.state==1: #sync
      if char!='\x7E':
        self.state = 2
        self.buf = self.buf + char

    elif self.state==2: #rx
      if char=='\x7E':
        self.state = 0
        self.rxpacket(self.buf)
      elif char=='\x7D':
        self.state = 3
      else:
        self.buf=self.buf + char

    elif self.state==3: #escape
      self.buf = self.buf + chr(ord(char) ^ 0x20)
      self.state = 2
    
  def loop(self):
    print "loop start"
    self.running = True
    while self.running:
      c=self.serial.read()
      if len(c)==0: continue
      self.manage(c)
      
    print "loop done"
      
  def start(self):
    print "starting receiver"
    self.thr = threading.Thread(target=self.loop)
    self.thr.start()
      
  def stop(self):
    print "stopping receiver"
    self.running = False
    self.serial.close()
    self.thr.join()
    
################################################################################

if len(sys.argv) != 2:
  print sys.argv[0] + " <port>"
  sys.exit(1)

def done():
  rx.stop()
  root.quit()

#init ui
root=tk.Tk()
root.protocol("WM_DELETE_WINDOW", done)
tlm=tlm(root,800)

#open serial
rx = Receiver(sys.argv[1], tlm)

#start rx thread
rx.start()

#ui loop
root.mainloop()

