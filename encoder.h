#ifndef __MOTNODE__ENCODER__H__
#define __MOTNODE__ENCODER__H__

struct motnode_encoder_s;

int motnode_encoderinit(FAR struct motnode_encoder_s **enc);
void motnode_encodershutdown(FAR struct motnode_encoder_s *enc);
int motnode_encoderposition(FAR struct motnode_encoder_s *enc, FAR int32_t *pos);

#endif /* __ENCODER__H__ */

