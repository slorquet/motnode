#ifndef __MOTNODE__CRC16__H__
#define __MOTNODE__CRC16__H__

#include <stdint.h>

#define CRC16_INIT 0xFFFF

uint16_t crc16(uint16_t prev, uint8_t data);

#endif /* __MOTNODE_CRC16__H__ */
