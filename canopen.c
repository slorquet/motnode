/****************************************************************************
 * canutils/motnode/canopen.c
 *
 *   Copyright (C) 2016 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <semaphore.h>
#include <nuttx/drivers/can.h>

#include "canutils/libcanopen/canopen.h"
#include "canutils/libcanopen/slave.h"

#include "canopen.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef CONFIG_MOTNODE_CANOPEN_DEVNAME
#error CANopen device not defined in config
#endif

#ifndef CONFIG_MOTNODE_CANOPEN_NODEID
#error CANopen default node ID not defined in config
#endif

/****************************************************************************
 * Private types
 ****************************************************************************/

/****************************************************************************
 * Global variables
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************/
static int txcb(FAR void *priv, FAR struct canmsg_s *msg)
{
  FAR struct motnode_canopen_s *privdata = (FAR struct motnode_canopen_s*)priv;
  struct can_msg_s nmsg;
  nmsg.cm_hdr.ch_dlc = msg->dlc;
  nmsg.cm_hdr.ch_id  = msg->id;
#ifdef CONFIG_CAN_EXTID
  nmsg.cm_hdr.ch_extid = 0;
#endif
  memcpy(nmsg.cm_data, msg->data, 8);
  return write(privdata->fd, &nmsg, CAN_MSGLEN(msg->dlc));
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************/
static void* motnode_canopenthread(void *ptr)
{
  FAR struct motnode_canopen_s *can = ptr;
  struct can_msg_s msgbuf;
  struct canmsg_s msg;
  int ret;

  fprintf(stderr, "CANopen loop started\n");
  can->running = true;
  while(can->running)
    {

      ret = read(can->fd, &msgbuf, sizeof(msgbuf));
      if(ret>0)
        {
          msg.id  = msgbuf.cm_hdr.ch_id;
          msg.dlc = msgbuf.cm_hdr.ch_dlc;
          memcpy(msg.data, msgbuf.cm_data, 8);
          canopen_slave_rx(&can->node, &msg);
        }
    }

  fprintf(stderr, "CANopen loop stopped\n");
  return NULL;
}

/****************************************************************************/
int motnode_caninit(struct motnode_canopen_s *can)
{
  int ret;

  /* Open the character driver that wraps the can interface */

  can->fd = open(CONFIG_MOTNODE_CAN_DEVNAME, O_RDWR);
  if(can->fd < 0)
    {
      fprintf(stderr, "Failed to open CAN device (errno=%d)\n",errno);
      return ERROR;
    }

  /* Initialize CANopen node */

  can->nodeid = CONFIG_MOTNODE_CANOPEN_NODEID;
  ret = canopen_slave_init(&can->node, can->nodeid, txcb, can);

  if(ret)
    {
      fprintf(stderr, "Node init failed\n");
      close(can->fd);
      return ERROR;
    }

  fprintf(stderr, "CANopen loop starting\n");
  pthread_create(&can->pid, NULL, motnode_canthread, can);

  return 0;
}

/****************************************************************************/
int motnode_canopensetid(struct motnode_canopen_s *can, uint8_t nodeid)
{
  can->nodeid = nodeid;
  return 0;
}

/****************************************************************************/
void motnode_canopenshutdown(struct motnode_canopen_s *can)
{
  can->running = false;
  fprintf(stderr, "CANopen loop stopping...\n");
  can->running = false;
  pthread_kill(can->pid, 1);
  pthread_join(can->pid, NULL);
  close(can->fd);
  return;
}

