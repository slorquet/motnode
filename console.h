#ifndef __MOTNODE__CONSOLE__H__
#define __MOTNODE__CONSOLE__H__

#include <stdbool.h>

#include "motor.h"

struct motnode_console_s;

int motnode_consoleinit(FAR struct motnode_console_s **cons, FAR struct motnode_motor_s *motor);
void motnode_consoleshutdown(FAR struct motnode_console_s *cons);
void motnode_consoleloop(FAR struct motnode_console_s *cons);

#endif /* __MOTNODE__CONSOLE__H__ */

