/* telemetry support */

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/boardctl.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <debug.h>
#include <string.h>
#include <termios.h>

#include "telemetry.h"
#include "crc16.h"

#define MOTNODE_TELEMETRY_DELIMITER 0x7E
#define MOTNODE_TELEMETRY_ESCAPE    0x7D
#define MOTNODE_TELEMETRY_ESCMASK   0x20

struct motnode_tlm_msg_s
{
  /*0*/ uint32_t frameindex;   /*frame index    count*/
  /*1*/ int32_t  encoderpos;   /*encoder pos    pulses*/
  /*2*/ b16_t    encoderrate;  /*encoder rate   pulses/10ms*/
  /*3*/ uint32_t pwmon    : 1;
  /* */ uint32_t cw       : 1;
  /* */ uint32_t closed   : 1;
  /* */ uint32_t pvt      : 1;
  /* */ uint32_t reserved : 28;
  /*4*/ b16_t    pwmrate;
  /*5*/ b16_t    speedsetpt;
} __attribute__((packed));

/* Telemetry
 *
 * This is used to report internal status info to an external supervisor.
 * We are using an UART at a non-standard high speed.
 * at 1 Mbps, each bit lasts 1 us, a char uses 10 us. In a 10 ms period there is room for max. 1000 chars.
 * at 115200 bauds, each bit lasts 8.6 us, a char uses 86 us. in a 1 ms period there is room for max 116 chars.
 */

struct motnode_tlm_s
{
  int fd;
  FAR struct motnode_motor_s *motor;
  struct motnode_tlm_msg_s msg;
};

/****************************************************************************/
int motnode_tlminit(FAR struct motnode_tlm_s **tlm, FAR struct motnode_motor_s *motor)
{
  FAR struct motnode_tlm_s *t = malloc(sizeof(struct motnode_tlm_s));
  const char *path = CONFIG_MOTNODE_TELEMETRY_DEVNAME;
  struct termios term;
  
  if (t == NULL)
    {
      fprintf(stderr, "Failed to alloc telemetry device\n");
      return ERROR;
    }

  /* Open the telemetry device for writing */

  t->fd = open(path, O_WRONLY);
  if (t->fd < 0)
    {
      fprintf(stderr, "tlm: open %s failed: %d\n", path, errno);
close_free:
      free(t);
      return ERROR;
    }

  /* Set the baud rate CONFIG_MOTNODE_TELEMETRY_BAUDRATE */
  if(tcgetattr(t->fd, &term) != OK)
    {
      fprintf(stderr, "tlm: tcgetattr failed: %d\n", errno);
      goto close_free;
    }

  if(cfsetospeed(&term, B1000000) != OK)
    {
      fprintf(stderr, "tlm: cfsetspeed failed: %d\n", errno);
      goto close_free;
    }
    
  if(tcsetattr(t->fd, TCSANOW, &term) != OK)
    {
      fprintf(stderr, "tlm: tcsetattr failed: %d\n", errno);
      goto close_free;
    }

  t->motor = motor;
  motor->tlm = t;
  t->msg.frameindex = 0;
  t->msg.reserved   = 0;
  *tlm = t;

  return 0;
}

/****************************************************************************/
void motnode_tlmshutdown(FAR struct motnode_tlm_s *tlm)
{
  close(tlm->fd);
}

/****************************************************************************/
static inline void sendesc(int fd, uint8_t buf[4])
{
  if (buf[0] != MOTNODE_TELEMETRY_DELIMITER && buf[0] != MOTNODE_TELEMETRY_ESCAPE)
    {
      write(fd, buf, 1);
    }
  else
    {
      buf[1] = buf[0] ^ MOTNODE_TELEMETRY_ESCMASK;
      buf[0] = MOTNODE_TELEMETRY_ESCAPE;
      write(fd, buf, 2);
    }
}

/****************************************************************************/
/* send current motor info. TLM frame format:
 * 1 byte header 0xFE
 * Payload data, 20 bytes
 * CRC, 2 bytes
 * Data is escaped to avoid presence of flags within data
 * 7E sent as 7D5E
 * 7D sent as 7D5D
 * Other escape sequences are illegal and make decoder wait for next frame
 */
void motnode_tlmsendframe(struct motnode_tlm_s *tlm)
{
  uint8_t buf[4];
  int i;
  uint16_t crc;

  /* encode */
  tlm->msg.encoderpos  = tlm->motor->newpos;
  tlm->msg.encoderrate = tlm->motor->rate;
  tlm->msg.pwmon       = (uint32_t)tlm->motor->direction != MOTNODE_DIR_STOPPED;
  tlm->msg.cw          = (uint32_t)tlm->motor->direction == MOTNODE_DIR_CW;
  tlm->msg.closed      = (uint32_t)tlm->motor->mode != MOTNODE_CONTROL_OPENLOOP;
  tlm->msg.pvt         = (uint32_t)tlm->motor->mode == MOTNODE_CONTROL_PVT;
  tlm->msg.pwmrate     = (uint32_t)tlm->motor->command;
  tlm->msg.speedsetpt  = (uint32_t)tlm->motor->v_setpt_cur;
  
  /* SOF */
  buf[0] = MOTNODE_TELEMETRY_DELIMITER;
  write(tlm->fd, buf,1);

  crc = CRC16_INIT;
  /* Payload */
  for(i=0;i<sizeof(tlm->msg);i++)
    {
      buf[0] = ((uint8_t*)&tlm->msg)[i];
      crc = crc16(crc, buf[0]);
      sendesc(tlm->fd, buf);
    }

  /* FCS */
  buf[0] = (uint8_t)(crc & 0xFF);
  sendesc(tlm->fd, buf);
  buf[0] = crc>>8;
  sendesc(tlm->fd, buf);

  /* EOF */
  buf[0] = MOTNODE_TELEMETRY_DELIMITER;
  write(tlm->fd, buf,1);

  tlm->msg.frameindex++;
}

