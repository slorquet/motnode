#include "crc16.h"

#include <stdint.h>

uint16_t crc16(uint16_t crc, uint8_t data)
{
  crc ^= 0xFFFF;

  crc ^= data&0xFF;
  crc ^= (crc<<4)&0xFF;
  crc  = (uint16_t)(crc>>8)^
    (uint16_t)((uint16_t)(crc&0xFF)<<8)^
    (uint16_t)((uint16_t)(crc&0xFF)<<3)^
    (uint16_t)((crc&0xFF)>>4);
  crc ^= 0xFFFF;

  return crc;
}

