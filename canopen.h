#ifndef __MOTNODE__CANOPEN__H__
#define __MOTNODE__CANOPEN__H__

#include <stdbool.h>

struct motnode_canopen_s
  {
    int fd;
    pthread_t pid;
    struct canopen_slave_s node;
    uint8_t nodeid;
    bool running;
  };

int motnode_canopeninit(struct motnode_canopen_s *can);
int motnode_canopensetid(struct motnode_canopen_s *can, uint8_t id);
void motnode_canopenshutdown(struct motnode_canopen_s *can);

#endif /*__MOTNODE__CAN__H__ */

