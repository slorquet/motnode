/****************************************************************************
 * examples/pwm/pwm_main.c
 *
 *   Copyright (C) 2011-2012, 2015 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/boardctl.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <debug.h>
#include <string.h>
#include <fixedmath.h>

#include <nuttx/drivers/pwm.h>

#include "pwm.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Configuration ************************************************************/

#ifndef CONFIG_PWM_MULTICHAN
#error multichan required
#endif

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct motnode_pwm_s
{
  int       fd;
  bool      enabled;
  uint32_t  freq;
  b16_t     duty[2];
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************/
int motnode_pwminit(FAR struct motnode_pwm_s **pwm)
{
  int ret;
  const char *path = CONFIG_MOTNODE_PWM_DEVNAME;
  FAR struct motnode_pwm_s *p = malloc(sizeof(struct motnode_pwm_s));

  if (p == NULL)
    {
      fprintf(stderr, "Failed to alloc pwm device\n");
      return ERROR;
    }

  /* Open the PWM device for reading */

  p->fd = open(path, O_RDONLY);
  if (p->fd < 0)
    {
      fprintf(stderr, "pwm: open %s failed: %d\n", path, errno);
      free(p);
      return ERROR;
    }
  p->enabled = 2; /* Invalid boolean to avoid warning at first state set */
  
  *pwm = p;

  return 0;
}

/****************************************************************************/
void motnode_pwmshutdown(FAR struct motnode_pwm_s *pwm)
{
  close(pwm->fd);
}

/****************************************************************************/
static int motnode_pwmapply(FAR struct motnode_pwm_s *pwm)
{
  struct pwm_info_s info;
  int ret;

  info.frequency = pwm->freq;

  info.channels[0].channel = 1;
  info.channels[0].duty    = pwm->duty[0];

  info.channels[1].channel = 2;
  info.channels[1].duty    = pwm->duty[1];

  ret = ioctl(pwm->fd, PWMIOC_SETCHARACTERISTICS, (unsigned long)((uintptr_t)&info));
  if (ret < 0)
    {
      printf("pwm: ioctl(PWMIOC_SETCHARACTERISTICS) failed: %d\n", errno);
      return ret;
    }

  return 0;
}

/****************************************************************************/
int motnode_pwmsetfreq(FAR struct motnode_pwm_s *pwm, uint32_t freq)
{
  pwm->freq = freq;

  return motnode_pwmapply(pwm);
}

/****************************************************************************/
int motnode_pwmsetrates(FAR struct motnode_pwm_s *pwm, b16_t rate0, b16_t rate1)
{

  pwm->duty[0] = rate0;
  pwm->duty[1] = rate1;

  return motnode_pwmapply(pwm);
}

/****************************************************************************/
int motnode_pwmsetstate(FAR struct motnode_pwm_s *pwm, bool enable)
{
  int ret=OK;

  if (enable)
    {
      if(!pwm->enabled)
        {
          ret = ioctl(pwm->fd, PWMIOC_START, 0);
          if (ret < 0)
            {
              fprintf(stderr, "pwm: ioctl(PWMIOC_START) failed: %d\n", errno);
            }
          else
            {
              pwm->enabled = true;
            }
        }
    }
  else
    {
      if(pwm->enabled)
        {
          ret = ioctl(pwm->fd, PWMIOC_STOP, 0);
          if (ret < 0)
            {
              fprintf(stderr, "pwm: ioctl(PWMIOC_STOP) failed: %d\n", errno);
            }
          else
            {
              pwm->enabled = false;
            }
        }
    }
  return ret;
}

/****************************************************************************/
int motnode_pwmgetfreq(FAR struct motnode_pwm_s *pwm, FAR uint32_t *freq)
{
  *freq = pwm->freq;
  return 0;
}

/****************************************************************************/
int motnode_pwmgetrates(FAR struct motnode_pwm_s *pwm, FAR b16_t *rate0, FAR b16_t *rate1)
{
  *rate0 = pwm->duty[0];
  *rate1 = pwm->duty[1];
  return 0;
}

/****************************************************************************/
int motnode_pwmgetstate(FAR struct motnode_pwm_s *pwm, FAR bool *enabled)
{
  *enabled = pwm->enabled;
  return 0;
}

