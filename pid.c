/* the bible
 * http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/
 */

#include "pid.h"

/****************************************************************************/
void motnode_pidinit(FAR struct motnode_pid_s *pid, uint32_t period_us)
{
  pid->lastinput = 0;
  pid->iterm     = 0;
  pid->period    = period_us;
}

/****************************************************************************/
void motnode_pidlimits(FAR struct motnode_pid_s *pid, b16_t low, b16_t high)
{
  if(low>high) return;
  pid->outmin = low;
  pid->outmax = high;

  if(pid->output > high) pid->output = high;
  if(pid->output < low ) pid->output = low;
  if(pid->iterm  > high) pid->iterm  = high;
  if(pid->iterm  < low ) pid->iterm  = low;
}

/****************************************************************************/
int motnode_pidupdate(FAR struct motnode_pid_s *pid, b16_t *output, b16_t setpoint, b16_t input)
{
  b16_t error = setpoint - input;
  b16_t dInput;

  pid->input = input;

  /* Compute derivative term */
  dInput = input - pid->lastinput;
  pid->lastinput = input;

  /* Compute Integral term */
  pid->iterm += b16mulb16(pid->Ki,error);
  if(pid->iterm  > pid->outmax) pid->iterm  = pid->outmax;
  if(pid->iterm  < pid->outmin) pid->iterm  = pid->outmin;

  /* Compute PID Output */
  pid->output = b16mulb16(pid->Kp,error) + pid->iterm - b16mulb16(pid->Kd,dInput);
  if(pid->output > pid->outmax) pid->output = pid->outmax;
  if(pid->output < pid->outmin) pid->output = pid->outmin;

  *output = pid->output;
  return 0;
}

