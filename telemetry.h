#ifndef __MOTNODE__TELEMETRY__H__
#define __MOTNODE__TELEMETRY__H__

#include "motor.h"

struct motnode_tlm_s;

int motnode_tlminit(FAR struct motnode_tlm_s **tlm, FAR struct motnode_motor_s *motor);
void motnode_tlmshutdown(FAR struct motnode_tlm_s *tlm);
void motnode_tlmsendframe(struct motnode_tlm_s *tlm);

#endif /* __MOTNODE__TELEMETRY__H__ */

