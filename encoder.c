/****************************************************************************
 * examples/qe/qe_main.c
 *
 *   Copyright (C) 2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/sensors/qencoder.h>

#include "encoder.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private Types
 ****************************************************************************/

struct motnode_encoder_s
{
  int fd;
  int32_t position;
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************/
int motnode_encoderinit(FAR struct motnode_encoder_s **enc)
{
  int ret;
  const char *path = CONFIG_MOTNODE_ENCODER_DEVNAME;
  FAR struct motnode_encoder_s *e = malloc(sizeof(struct motnode_encoder_s));

  if (e == NULL)
    {
      fprintf(stderr, "Failed to alloc pwm device\n");
      return ERROR;
    }


  e->fd = open(path, O_RDONLY);
  if (e->fd < 0)
    {
      fprintf(stderr, "qe: open %s failed: %d\n", path, errno);
      free(e);
      return ERROR;
    }

  ret = ioctl(e->fd, QEIOC_RESET, 0);
  if (ret < 0)
    {
      fprintf(stderr, "qe: ioctl(QEIOC_RESET) failed: %d\n", errno);
      close(e->fd);
      free(e);
      return ERROR;
    }

  *enc = e;
  
  return 0;
}

/****************************************************************************/
void motnode_encodershutdown(FAR struct motnode_encoder_s *enc)
{
  close(enc->fd);
  return;
}

/****************************************************************************/
int motnode_encoderposition(FAR struct motnode_encoder_s *enc, FAR int32_t *pos)
{
  int ret;
  /* Get the positions data using the ioctl */

  ret = ioctl(enc->fd, QEIOC_POSITION, (unsigned long)((uintptr_t)pos));
  if (ret < 0)
    {
      printf("qe: ioctl(QEIOC_POSITION, fd %d) failed: %d\n", enc->fd, errno);
      return EXIT_FAILURE;
    }
  
  return 0;
}
