/****************************************************************************
 * motnode/timer.c
 *
 *   Copyright (C) 2015 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>

#include <nuttx/semaphore.h>
#include <nuttx/timers/timer.h>

#include "timer.h"
#include "motor.h"
#include "encoder.h"
#include "telemetry.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#ifndef CONFIG_MOTNODE_TIMER_DEVNAME
#error you did not configure the timer device name. Run make menuconfig.
#endif

#ifndef CONFIG_MOTNODE_TIMER_INTERVAL
#error you did not configure the timer interval. Run make menuconfig.
#endif

#define CONFIG_MOTNODE_TIMER_SIGNO 1

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * motnode_timerhandler
 ****************************************************************************/
/* remember 
 *   CONFIG_MOTNODE_ENCODER_PULSES
 *   CONFIG_MOTNODE_TIMER_INTERVAL
 */

/****************************************************************************/
static void* motnode_timerthread(void *arg)
{
  sigset_t set;
  siginfo_t info;
  int ret;
  b16_t delta_v;
  uint32_t prevfreq;
  
  sigfillset(&set);
  FAR struct motnode_timer_s *tim   = arg;
  FAR struct motnode_motor_s *motor = tim->motor;
  
  fprintf(stderr, "Started timer thread\n");

  motnode_pidinit  (&motor->pid_speed, CONFIG_MOTNODE_TIMER_INTERVAL);
  motnode_pidlimits(&motor->pid_speed, 0, b16ONE-1);

  prevfreq = motor->freq;
  tim->running = true;
  while(tim->running)
    {
      sigwaitinfo(&set, &info);
      
      /*--------------------*/
      /* Read the encoder */
      /*--------------------*/
      motor->oldpos = motor->newpos;
      ret = motnode_encoderposition(motor->encoder, &motor->newpos);
      if(ret != 0)
        {
        /* TODO control failure */
          motor->direction = MOTNODE_DIR_STOPPED;
        }

      /*--------------------*/
      /* Compute encoder rate */
      /*--------------------*/

      motor->rate = itob16(motor->newpos - motor->oldpos);

      /*--------------------*/
      /* Compute rate sliding average */
      /*--------------------*/

      /* TODO */
      
      /*--------------------*/
      /* Compute target speed setpoint */
      /*--------------------*/

      delta_v = motor->v_setpt_tgt - motor->v_setpt_cur; /* this is the speed difference between target and current setpoints */
      if (delta_v > 0)
        {
          /* We are accelerating */
          if(delta_v > motor->accel)
            {
              delta_v = motor->accel; /* Limit to max acceleration */
            }
        }
      else if (delta_v < 0)
        {
          /* We are decelerating */
          if (delta_v < -motor->decel)
            {
              delta_v = -motor->decel; /* Limit to max deceleration (braking) */
            }
        }
      motor->v_setpt_cur += delta_v; /* Apply acceleration or deceleration */

      /*--------------------*/
      /* Compute output */
      /*--------------------*/
      if(motor->mode == MOTNODE_CONTROL_OPENLOOP)
        {
          /* In open loop mode command is directly given by user */
        }
      else if(motor->mode == MOTNODE_CONTROL_SPEED)
        {
          /* In speed mode a simple PID loop is used to control the rotation speed */
          if(motor->direction != MOTNODE_DIR_STOPPED) /*Do not update PID if motor is stopped */
            {
              motnode_pidupdate(&motor->pid_speed, &motor->command, motor->v_setpt_cur, motor->rate);
            }
        }
      else if(motor->mode == MOTNODE_CONTROL_PVT)
        {
          /* Some complex stuff is required to drive the shaft position */
          motor->command = 0;
        }
      else /* unknown mode */
        {
          motor->command = 0;
        }

      /*--------------------*/
      /* Apply the speed command */
      /*--------------------*/
      if (motor->direction == MOTNODE_DIR_STOPPED)
        {
          motnode_pwmsetstate(motor->pwm, false);
          motnode_pwmsetrates(motor->pwm, 0, 0);
        }
      else
        {
          motnode_pwmsetstate(motor->pwm, true);
          if(motor->direction == MOTNODE_DIR_CW)
            {
              motnode_pwmsetrates(motor->pwm, 0, motor->command);
            }
          else /* CCW */
            {
              motnode_pwmsetrates(motor->pwm, motor->command, 0);
            }
        }

      /*--------------------*/
      /*update pwm freq if required*/
      /*--------------------*/
      if(motor->freq != prevfreq)
        {
          motnode_pwmsetfreq(motor->pwm, motor->freq);
          prevfreq = motor->freq;
        }
 
      motnode_tlmsendframe(motor->tlm);

    } /* while */

  fprintf(stderr, "Stopped timer thread\n");
  return NULL;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************/
int motnode_timerinit(FAR struct motnode_timer_s **tim, FAR struct motnode_motor_s *mot)
{
  struct timer_notify_s notify;
  int ret;

  FAR struct motnode_timer_s *t = malloc(sizeof(struct motnode_timer_s));

  if (t == NULL)
    {
      fprintf(stderr, "Failed to alloc console\n");
      return ERROR;
    }

  /* Initialize the timer thread */

  fprintf(stderr,"Starting timer thread...\n");

  t->motor = mot;
  pthread_create(&t->pid, NULL, motnode_timerthread, t);
  pthread_setschedprio(t->pid, PTHREAD_DEFAULT_PRIORITY + 10);

  /* Open the timer device */

  fprintf(stderr, "Open %s\n", CONFIG_MOTNODE_TIMER_DEVNAME);

  t->fd = open(CONFIG_MOTNODE_TIMER_DEVNAME, O_RDONLY);
  if (t->fd < 0)
    {
      fprintf(stderr, "ERROR: Failed to open %s: %d\n",
              CONFIG_MOTNODE_TIMER_DEVNAME, errno);
      goto end_thread;
    }

  /* Set the timer interval */

  fprintf(stderr, "Set timer interval to %lu us\n",
         (unsigned long)CONFIG_MOTNODE_TIMER_INTERVAL);

  ret = ioctl(t->fd, TCIOC_SETTIMEOUT, CONFIG_MOTNODE_TIMER_INTERVAL);
  if (ret < 0)
    {
      fprintf(stderr, "ERROR: Failed to set the timer interval: %d\n", errno);
      goto end_close;
    }

  fprintf(stderr, "Enable timer notification\n");

  notify.signo = 1;
  notify.pid   = t->pid;
  notify.arg   = 0;

  ret = ioctl(t->fd, TCIOC_NOTIFICATION, (unsigned long)((uintptr_t)&notify));
  if (ret < 0)
    {
      fprintf(stderr, "ERROR: Failed to set the timer handler: %d\n", errno);
      goto end_close;
    }

  /* Start the timer */

  fprintf(stderr, "Start the timer\n");

  ret = ioctl(t->fd, TCIOC_START, 0);
  if (ret < 0)
    {
      fprintf(stderr, "ERROR: Failed to start the timer: %d\n", errno);
      goto end_close;
    }

  *tim = t;

  return 0;

end_close:
  close(t->fd);

end_thread:
  t->running = false;
  pthread_kill(t->pid, 2);
  pthread_join(t->pid, NULL);
  free(t);

  return ERROR;
}

/****************************************************************************/
void motnode_timershutdown(FAR struct motnode_timer_s *tim)
{
  int ret;

  /* Stop the timer */

  fprintf(stderr, "Stop the timer\n");

  ret = ioctl(tim->fd, TCIOC_STOP, 0);
  if (ret < 0)
    {
      fprintf(stderr, "ERROR: Failed to stop the timer: %d\n", errno);
    }

  /* Even if that failed, close the device. */
  close(tim->fd);

  fprintf(stderr, "Stopping timer thread\n");

  tim->running = 0;
  pthread_kill(tim->pid, 1);
  pthread_join(tim->pid, NULL);

  return;
}

