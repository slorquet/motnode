#ifndef __MOTNODE__PID__H__
#define __MOTNODE__PID__H__

#include <fixedmath.h>

struct motnode_pid_s
{
  b16_t Kp;
  b16_t Ki;
  b16_t Kd;

  /*Limits*/
  b16_t outmin;
  b16_t outmax;

  /*State*/
  uint32_t period;
  b16_t    input;
  b16_t    lastinput;
  b16_t    iterm;
  b16_t    output;
};

void motnode_pidinit(FAR struct motnode_pid_s *pid, uint32_t period_us);
void motnode_pidlimits(FAR struct motnode_pid_s *pid, b16_t low, b16_t high);
int motnode_pidupdate(FAR struct motnode_pid_s *pid, b16_t *output, b16_t setpoint, b16_t input);

#endif /* __MOTNODE__PID__H__ */
